# ws@2024

# ###############################################################################
# Define Local Values in Terraform
# ###############################################################################

locals {

  # Region
  aws_region      = "<region>"

  # Application
  name                   = "<name>"
  cut_name               = "<name>"

  # Environment [info]
  environment            = "prod"
  ownerEmail             = "adm+<name>@domain.com"
  team                   = "DevOps"
  deployedby             = "Terraform"

  # Domain
  domain_name_default = "domain.com"
  domain_name_first   = "dev.domain.com"
  domain_name_second  = "demo.domain.com"

  locals_tags = {
    owners      = local.ownerEmail
    environment = local.environment
  }
} 

