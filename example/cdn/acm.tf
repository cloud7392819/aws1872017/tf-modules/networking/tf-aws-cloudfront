# ws@2024

# ###############################################################################
# Terraform Remote State Datasource
# ###############################################################################

data "terraform_remote_state" "acm" {
  backend = "s3"
  config = {
    bucket = "<name>-tf-remote-state-all-<region>"
    key    = "tfstate/<region>/acm/terraform.tfstate"
    region = "<region>"
  }
}

