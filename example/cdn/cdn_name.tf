# ws@2023

# #################################################################
# CloudFront: <cdn_name>
# ENV: <env>
# #################################################################

module "cdn_name" {

  source  = "git::https://gitlab.com/cloud7392819/aws1872017/tf-modules/networking/cloudfront/tf-aws-cloudfront.git?ref=tags/x.x.x"
  #source = "./modules/cdn_name"

  # #################################################################
  # Common/General settings
  # #################################################################

  # Whether to create the resources (`false` prevents the module from creating any resources).
  module_enabled = true

  # Application name
  name = local.cut_name

  # Domain name
  domain_name = local.domain_name_default

  # S3 bucket
  s3_bucket_name = "<cdn_name>.${local.domain_name_default}"

  # Emulation of `depends_on` behavior forthe module.
  # Non zero length string can be used to have current module wait for the specified resource.
  module_depends_on = ""

  # Additional mapping of tags to assign to the all linked resources.
  module_tags = {
    NAME        = "<cdn_name>.${local.domain_name_default}"
    ManagedBy   = "Terraform"
    "ENV"       = "<env>"
  }

  # #################################################################
  # Cloudfront settings
  # #################################################################
   
  # (optional) Default root object 
  # The object (file name) to return when a viewer requests the root URL (/) instead of a specific object.
  default_root_object = "index.html"

  # Choose the price class associated with the maximum price that you want to pay
  # https://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/PriceClass.html
  # -- "PriceClass_all" Use all edge locations (best performance)
  # -- "PriceClass_100" Use only North America and Europe
  # -- "PriceClass_200" Use North America, Europe, Asia, Middle East, and Africa
  price_class = "PriceClass_100"

  # #################################################################
  # ACM, Route53
  # #################################################################
  acm_enabled = true
  #acm_certificate_arn = data.terraform_remote_state.acm.outputs.<domain>_certificate_arn
  #acm_certificate_arn = "arn"

  # #################################################################
  # Trusted Key Group
  # #################################################################
  keys_enabled = true

  # openssl genrsa -out xxx_cdn_private.pem 2048:
  # * openssl: This is the command to call OpenSSL, a tool for managing SSL/TLS certificates and key generation.
  # * genrsa: This is an OpenSSL subcommand for generating RSA key pairs.
  # * -out xxx_cdn_private.pem: The -out flag tells OpenSSL where to write the output. 
  #   In this case, the private key will be saved to the file xxx_cdn_private.pem.
  # * 2048: This is the number of bits in the generated private key. 2048 is a standard value for modern security systems.
  
  # openssl rsa -pubout -in xxx_cdn_private.pem -out xxx_cdn_public.pem:
  # * openssl: This is the command to call OpenSSL.
  # * rsa: This is an OpenSSL subcommand for working with RSA keys.
  # * -pubout: This flag tells OpenSSL to output a public key from a private key.
  # * -in xxx_cdn_private.pem: The -in flag tells OpenSSL where to take the input from. 
  #   In this case, it's taken from the file xxx_cdn_private.pem.
  # * -out aisoniq_cdn_public.pem: The -out flag tells OpenSSL where to write the output. 
  #   In this case, the public key will be saved in the file xxx_cdn_public.pem.

  public_keys = file("./keys/dra_cdn_public.pem")

}
