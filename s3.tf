# ws@2023 s3.tf

##################################################################
# S3
##################################################################

resource "aws_s3_bucket" "default" {

  # This allows instances to be created, updated, and deleted. 
  # Instances also support provisioning: https://www.terraform.io/docs/provisioners/index.html
  count = local.create ? 1 : 0  

  bucket          = var.s3_bucket_name

  provider        = aws.virginia
  
  force_destroy   = true

  lifecycle {
    prevent_destroy = false
  }

  tags = merge(
    local.tags,
    {
      Name   = "${var.s3_bucket_name}" 
      Region = "us-east-1"       
    }
  )

}

resource "aws_s3_bucket_public_access_block" "default" {

  # This allows instances to be created, updated, and deleted. 
  # Instances also support provisioning: https://www.terraform.io/docs/provisioners/index.html
  count = local.create ? 1 : 0  
  
  bucket          = join("", aws_s3_bucket.default[*].id)

  provider        = aws.virginia
  
  block_public_acls         = true
  block_public_policy       = true
  restrict_public_buckets   = true
  ignore_public_acls        = true
}


resource "aws_s3_bucket_policy" "default" {

  # This allows instances to be created, updated, and deleted. 
  # Instances also support provisioning: https://www.terraform.io/docs/provisioners/index.html
  count = local.create ? 1 : 0  

  bucket    = join("", aws_s3_bucket.default[*].id)

  provider  = aws.virginia
  
  policy    = data.aws_iam_policy_document.default.json
}

