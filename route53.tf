# ws@2023 route53.tf


resource "aws_route53_record" "cert_validation_default" {

  # This allows instances to be created, updated, and deleted. 
  # Instances also support provisioning: https://www.terraform.io/docs/provisioners/index.html
  count = (local.create && var.acm_enabled) ? 1 : 0 

  allow_overwrite = true

  name = tolist(aws_acm_certificate.default[count.index].domain_validation_options)[0].resource_record_name
  
  records = [ 
    tolist(aws_acm_certificate.default[count.index].domain_validation_options)[0].resource_record_value 
  ]
  
  type = tolist(aws_acm_certificate.default[count.index].domain_validation_options)[0].resource_record_type
  
  zone_id = join("", data.aws_route53_zone.default.*.id)
  
  ttl = 60

  depends_on = [
    aws_s3_bucket.default
  ]
}


resource "aws_route53_record" "default" {

  # This allows instances to be created, updated, and deleted. 
  # Instances also support provisioning: https://www.terraform.io/docs/provisioners/index.html
  count = (local.create && var.acm_enabled && !var.keys_enabled) ? 1 : 0   

  name = "${var.s3_bucket_name}"

  zone_id = join("", data.aws_route53_zone.default.*.id)

  type = "A"

  alias {

    name    = join("", aws_cloudfront_distribution.default.*.domain_name)

    zone_id = join("", aws_cloudfront_distribution.default.*.hosted_zone_id)

    evaluate_target_health = false
  }

  depends_on = [
    aws_s3_bucket.default
  ]
}



resource "aws_route53_record" "default_trusted_key_groups" {

  # This allows instances to be created, updated, and deleted. 
  # Instances also support provisioning: https://www.terraform.io/docs/provisioners/index.html
  count = (local.create && var.acm_enabled && var.keys_enabled) ? 1 : 0   

  name = "${var.s3_bucket_name}"

  zone_id = join("", data.aws_route53_zone.default.*.id)

  type = "A"

  alias {

    name    = join("", aws_cloudfront_distribution.default_trusted_key_groups.*.domain_name)

    zone_id = join("", aws_cloudfront_distribution.default_trusted_key_groups.*.hosted_zone_id)

    evaluate_target_health = false
  }

  depends_on = [
    aws_s3_bucket.default
  ]
}