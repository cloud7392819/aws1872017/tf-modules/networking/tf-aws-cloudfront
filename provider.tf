# ws@2023 provider.tf

provider "aws" {

  region = var.region

  # NOTE: Should only be used for testing unless you know what you're doing
  skip_credentials_validation = true
  skip_region_validation      = true
  skip_metadata_api_check     = true

}

provider "aws" {

  alias  = "virginia"
  region = "us-east-1"

  # NOTE: Should only be used for testing unless you know what you're doing
  skip_credentials_validation = true
  skip_region_validation      = true
  skip_metadata_api_check     = true
}