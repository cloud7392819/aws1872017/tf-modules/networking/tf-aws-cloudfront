# ws@2023 outputs.tf

output "distribution_id" {
  value = aws_cloudfront_distribution.default.*.id
}

output "distribution_arn" {
  value = aws_cloudfront_distribution.default.*.arn
}

output "distribution_caller_reference" {
  value = aws_cloudfront_distribution.default.*.caller_reference
}

output "distribution_status" {
  value = aws_cloudfront_distribution.default.*.status
}

output "distribution_trusted_signers" {
  value = aws_cloudfront_distribution.default.*.trusted_signers
}

output "distribution_domain_name" {
  value = aws_cloudfront_distribution.default.*.domain_name
}

output "distribution_last_modified_time" {
  value = aws_cloudfront_distribution.default.*.last_modified_time
}

output "distribution_in_progress_validation_batches" {
  value = aws_cloudfront_distribution.default.*.in_progress_validation_batches
}

output "distribution_etag" {
  value = aws_cloudfront_distribution.default.*.etag
}

output "distribution_hosted_zone_id" {
  value = aws_cloudfront_distribution.default.*.hosted_zone_id
}

#output "origin_access_identity_id" {
#  value = var.origin_access_identity_enabled == true ? aws_cloudfront_origin_access_identity.default.*.id : null
#}

#output "origin_access_identity_iam_arn" {
#  value = var.origin_access_identity_enabled == true ? aws_cloudfront_origin_access_identity.default.*.iam_arn : null
#}

#output "monitoring_subscription_id" {
#  value = var.monitoring_subscription_enabled == true ? aws_cloudfront_monitoring_subscription.default.*.id : null
#}

#output "origin_access_control_id" {
#  value = var.origin_access_control_enabled ? aws_cloudfront_origin_access_control.default.*.id : null
#}

