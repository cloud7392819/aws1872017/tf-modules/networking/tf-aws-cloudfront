# ws@2023 data.tf

##################################################################
# DATA
##################################################################


data "aws_iam_policy_document" "default" {

  statement {

    actions = [
      "s3:GetObject"
    ]

    resources = [
      "${join("", aws_s3_bucket.default[*].arn)}/*"
    ]

    principals {

      type        = "AWS"
      
      identifiers = [ 
        join("", aws_cloudfront_origin_access_identity.default[*].iam_arn) 
      ]
    }
  }

  depends_on = [
    aws_s3_bucket.default
  ]
}

##################################################################
# --- Route53 ---
##################################################################

data "aws_route53_zone" "default" {

  # This allows instances to be created, updated, and deleted. 
  # Instances also support provisioning: https://www.terraform.io/docs/provisioners/index.html
  count = ( local.create && var.acm_enabled ) ? 1 : 0  

  name = var.domain_name
}



