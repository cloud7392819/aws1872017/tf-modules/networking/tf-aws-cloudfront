![Terraform](https://lgallardo.com/images/terraform.jpg)

# tf-aws-cloudfront
---
This module manage AWS .. [terraform-module-aws-cloudfront].

## MODULE

```hcl
module "cloudfront_<cdn_name>" {

  #source = "git::<url>?ref=<tag>"
  source  = "git::https://gitlab.com/cloud7392819/aws1872017/tf-modules/networking/cloudfront/tf-aws-cloudfront.git?ref=tags/0.0.x"
  #source = "./modules/module_<cdn_name>"

  ##################################################################
  # Common/General settings
  ##################################################################

  # Whether to create the resources (`false` prevents the module from creating any resources).
  module_enabled = false

  # Application name
  name = var.cut_name

  # Domain name
  domain_name = var.domain_name_default

  # S3 bucket
  s3_bucket_name = "<cdn_name>.${var.domain_name_default}"

  # Emulation of `depends_on` behavior for the module.
  # Non zero length string can be used to have current module wait for the specified resource.
  module_depends_on = ""

  # Additional mapping of tags to assign to the all linked resources.
  module_tags = {
    ManagedBy = "Terraform"
    "ENV"     = "<env>"
  }

  ##################################################################
  # Cloudfront settings
  ##################################################################
   
  # (optional) Default root object 
  # The object (file name) to return when a viewer requests the root URL (/) instead of a specific object.
  default_root_object = "index.html"

  # Choose the price class associated with the maximum price that you want to pay
  # https://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/PriceClass.html
  # -- "PriceClass_all" Use all edge locations (best performance)
  # -- "PriceClass_100" Use only North America and Europe
  # -- "PriceClass_200" Use North America, Europe, Asia, Middle East, and Africa
  price_class = "PriceClass_200"

  ##################################################################
  # ACM, Route53
  ##################################################################
  
  acm_enabled = true
  ## if false
  acm_certificate_arn = ""

  ##################################################################
  # Trusted Key Group
  ##################################################################
  keys_enabled = true

  public_keys = file("./keys/xxx_cdn_public.pem")
  
}
```

## Usage

To run this example you need to execute:

```bash
$ terraform init
$ terraform plan
$ terraform apply
```

## Keys

```sh
openssl genrsa -out xxx_cdn_private.pem 2048:
```
```
* openssl: This is the command to call OpenSSL, a tool for managing SSL/TLS certificates and key generation.
* genrsa: This is an OpenSSL subcommand for generating RSA key pairs.
* -out xxx_cdn_private.pem: The -out flag tells OpenSSL where to write the output. 
In this case, the private key will be saved to the file xxx_cdn_private.pem.
* 2048: This is the number of bits in the generated private key. 2048 is a standard value for modern security systems.
```
```sh  
openssl rsa -pubout -in xxx_cdn_private.pem -out xxx_cdn_public.pem:
```
```
* openssl: This is the command to call OpenSSL.
* rsa: This is an OpenSSL subcommand for working with RSA keys.
* -pubout: This flag tells OpenSSL to output a public key from a private key.
* -in xxx_cdn_private.pem: The -in flag tells OpenSSL where to take the input from. 
In this case, it's taken from the file xxx_cdn_private.pem.
* -out aisoniq_cdn_public.pem: The -out flag tells OpenSSL where to write the output. 
In this case, the public key will be saved in the file xxx_cdn_public.pem.
```


<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.0 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | >= 5 |
| <a name="requirement_random"></a> [random](#requirement\_random) | >= 2.1 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | >= 5 |
| <a name="provider_random"></a> [random](#provider\_random) | >= 2.1 |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_xxx"></a> [cache\_bucket](#module\_cache\_bucket) | git::https://gitlab.com/xxxx | 6.0.0 |


## Resources

| Name | Type |
|------|------|

| [aws_iam_policy.default](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy) | resource |
| [aws_iam_policy.default_cache_bucket](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy) | resource |
| [aws_iam_role.default](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="module_enable"></a> [module\_enable](#input\_module\_enable) | Whether to create the resources | `bool` | `false` | true |
| <a name="module_depend_on"></a> [module\_depend\_on](#input\_module\_depend\_on) | Emulation of behavior for the module. | `string` | `""` | no |
| <a name="name"></a> [name](#input\_name) | Application name | `string` | `""` | name |
| <a name="module_tags"></a> [module\_tags](#input\_module\_tags) | Additional mapping of tags to assign to the all linked resources | `map(string)` | `{}` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_project_name"></a> [project\_name](#output\_project\_name) | Project name |
| <a name="output_role_arn"></a> [role\_arn](#output\_role\_arn) | IAM Role ARN |
| <a name="output_role_id"></a> [role\_id](#output\_role\_id) | IAM Role ID |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->

## Versioning
This repository follows [Semantic Versioning 2.0.0](https://semver.org/)

## Git Hooks
This repository uses [pre-commit](https://pre-commit.com/) hooks.

## URLs

1.
2.


##