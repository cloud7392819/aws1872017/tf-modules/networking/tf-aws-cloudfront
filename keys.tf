# ws@2023

resource "tls_private_key" "default" {

  # This allows instances to be created, updated, and deleted. 
  # Instances also support provisioning: https://www.terraform.io/docs/provisioners/index.html
  count = ( local.create && var.keys_enabled ) ? 1 : 0  

  algorithm = "RSA"
  rsa_bits  = 2048

}

resource "aws_cloudfront_public_key" "default" {

  # This allows instances to be created, updated, and deleted. 
  # Instances also support provisioning: https://www.terraform.io/docs/provisioners/index.html
  count = ( local.create && var.keys_enabled ) ? 1 : 0  

  provider = aws.virginia

  comment   = "${var.s3_bucket_name} public key"

  name      = "${local.s3_bucket_name}-key"

  #encoded_key = tls_private_key.default.*.public_key_pem
  encoded_key = var.public_keys

}

resource "aws_cloudfront_key_group" "default" {

  # This allows instances to be created, updated, and deleted. 
  # Instances also support provisioning: https://www.terraform.io/docs/provisioners/index.html
  count = ( local.create && var.keys_enabled ) ? 1 : 0  

  provider = aws.virginia

  comment = "${var.s3_bucket_name} key group"

  name  = "${local.s3_bucket_name}-key-group"

  items = [ join("", aws_cloudfront_public_key.default.*.id) ]

}

