# ws@2023 variables.tf

##################################################################
# Common/General settings
##################################################################

variable "module_enabled" {
  description = "Whether to create the resources."
  type        = bool
  # `false` prevents the module from creating any resources
  default     = false
}

# Application name
variable "name" {
	description = "Application name"
	default		  = null
}

# Domain name
variable "domain_name" {
  description = "The Domain name"
  default     = null
}

# Zone_id
variable "zone_id" {
  description = "The VPC ID"
  default     = null
}

# Region
variable "region" {
  description	= "AWS Region"
  default 		= null
}

variable "module_depends_on" {
  description = "Emulation of `depends_on` behavior for the module."
  type        = any
  # Non zero length string can be used to have current module wait for the specified resource.
  default     = ""
}

variable "module_tags" {
  description = "Additional mapping of tags to assign to the all linked resources."
  type        = map(string)
  default     = {}
}

##################################################################
# S3 Bucket
##################################################################

variable "s3_bucket_enabled" {
  description = "Whether to create the resources."
  type        = bool
  # `false` prevents the module from creating any resources
  default     = false
}

# Application name
variable "s3_bucket_name" {
	description = "S3 bucket name"
	default		  = null
}

##################################################################
# CloudFront
##################################################################


variable "price_class" {
	description = "Choose the price class associated with the maximum price that you want to pay"
	default		  = null
}

variable "default_root_object" {
	description = "The object (file name) to return when a viewer requests the root URL (/) instead of a specific object."
	default		  = "/"
}

##################################################################
## CloudFront Key Management 
##################################################################

variable "keys_enabled" {
  description = "Whether to create the resources."
  type        = bool
  # `false` prevents the module from creating any resources
  default     = false
}

variable "public_keys" {
	description = ""
	default		  = null
}

variable "keys_groups" {
	description = ""
	default		  = null
}

##################################################################
# ACM
##################################################################

variable "acm_enabled" {
  description = "Whether to create the resources."
  type        = bool
  # `false` prevents the module from creating any resources
  default     = false
}

variable "acm_certificate_arn" {
  description = ""
  #type       = string
  default     = null
}



