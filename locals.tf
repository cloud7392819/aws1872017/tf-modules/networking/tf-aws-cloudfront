# ws@2023 locals.tf

##################################################################
# LOCALS
##################################################################

locals {

  # create
  enabled = var.module_enabled ? true : false
  create  = var.module_enabled ? true : false

  #s3_bucket_name = "${replace(var.s3_bucket_name, "/\\.$/", "")}"
  s3_bucket_name = "${replace(var.s3_bucket_name, ".", "_")}"

  acm_certificate_arn  = var.acm_enabled ? join("", aws_acm_certificate.default.*.arn) : var.acm_certificate_arn

  # Trusted Key Group
  keys_groups = join("", aws_cloudfront_key_group.default.*.id) 
    
  tags = merge(
    var.module_tags,
    {
      Terraform = true
    }
  )
}


