# ws@2023 

resource "aws_cloudfront_origin_access_identity" "default" {

  # This allows instances to be created, updated, and deleted. 
  # Instances also support provisioning: https://www.terraform.io/docs/provisioners/index.html
  count = local.create ? 1 : 0  

  comment = "access-identity-${var.s3_bucket_name}.s3.amazonaws.com"

  depends_on = [
    aws_s3_bucket.default
  ]
}


resource "aws_cloudfront_distribution" "default" {

  # This allows instances to be created, updated, and deleted. 
  # Instances also support provisioning: https://www.terraform.io/docs/provisioners/index.html
  count = local.create && !var.keys_enabled ? 1 : 0  

  provider = aws.virginia

  origin {
    domain_name = join("", aws_s3_bucket.default.*.bucket_regional_domain_name)
    origin_id   = "s3-cloudfront"

    s3_origin_config {
      origin_access_identity = join("", aws_cloudfront_origin_access_identity.default.*.cloudfront_access_identity_path)
    }
  }

  enabled             = true

  is_ipv6_enabled     = true
  
  # (optional) Default root object 
  # The object (file name) to return when a viewer requests the root URL (/) instead of a specific object.
  default_root_object = var.default_root_object
  
  aliases = [
    "${var.s3_bucket_name}"
  ]
  
  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }
  
  default_cache_behavior {
    allowed_methods = [
      "DELETE", 
      "GET", 
      "HEAD", 
      "OPTIONS", 
      "PATCH", 
      "POST", 
      "PUT"
    ]
    cached_methods = [
      "GET", 
      "HEAD"
    ]
    target_origin_id = "s3-cloudfront"

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }

    viewer_protocol_policy = "redirect-to-https"
    min_ttl                = 0
    default_ttl            = 3600
    max_ttl                = 86400
  }

  # Choose the price class associated with the maximum price that you want to pay
  # https://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/PriceClass.html
  # -- "PriceClass_all" Use all edge locations (best performance)
  # -- "PriceClass_100" Use only North America and Europe
  # -- "PriceClass_200" Use North America, Europe, Asia, Middle East, and Africa
  price_class = var.price_class

  viewer_certificate {
    cloudfront_default_certificate  = false
    acm_certificate_arn             = local.acm_certificate_arn
    ssl_support_method              = "sni-only"
    minimum_protocol_version        = "TLSv1.2_2021"
  }

  custom_error_response {
    error_caching_min_ttl = "0"
    error_code            = "403"
    response_code         = "200"
    response_page_path    = "/"
  }

  custom_error_response {
    error_caching_min_ttl = "0"
    error_code            = "404"
    response_code         = "200"
    response_page_path    = "/"
  }

  custom_error_response {
    error_caching_min_ttl = "0"
    error_code            = "400"
    response_code         = "200"
    response_page_path    = "/"
  }
  
  depends_on = [
    aws_s3_bucket.default
  ]

  tags = merge(
    local.tags,
    {
      Name   = "${var.s3_bucket_name}" 
      Region = "us-east-1"       
    }
  )
}

##################################################################
# Add Trusted Key Group
##################################################################

resource "aws_cloudfront_distribution" "default_trusted_key_groups" {

  # This allows instances to be created, updated, and deleted. 
  # Instances also support provisioning: https://www.terraform.io/docs/provisioners/index.html
  count = local.create && var.keys_enabled ? 1 : 0  

  provider = aws.virginia

  origin {
    domain_name = join("", aws_s3_bucket.default.*.bucket_regional_domain_name)
    origin_id   = "s3-cloudfront"

    s3_origin_config {
      origin_access_identity = join("", aws_cloudfront_origin_access_identity.default.*.cloudfront_access_identity_path)
    }
  }

  enabled             = true

  is_ipv6_enabled     = true
  
  # (optional) Default root object 
  # The object (file name) to return when a viewer requests the root URL (/) instead of a specific object.
  default_root_object = var.default_root_object
  
  aliases = [
    "${var.s3_bucket_name}"
  ]
  
  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }
  
  default_cache_behavior {
    allowed_methods = [
      "DELETE", 
      "GET", 
      "HEAD", 
      "OPTIONS", 
      "PATCH", 
      "POST", 
      "PUT"
    ]
    cached_methods = [
      "GET", 
      "HEAD"
    ]
    target_origin_id = "s3-cloudfront"

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }

    trusted_key_groups = [ "${local.keys_groups}" ]

    viewer_protocol_policy = "redirect-to-https"
    min_ttl                = 0
    default_ttl            = 3600
    max_ttl                = 86400
  }

  # Choose the price class associated with the maximum price that you want to pay
  # https://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/PriceClass.html
  # -- "PriceClass_all" Use all edge locations (best performance)
  # -- "PriceClass_100" Use only North America and Europe
  # -- "PriceClass_200" Use North America, Europe, Asia, Middle East, and Africa
  price_class = var.price_class

  viewer_certificate {
    cloudfront_default_certificate  = false
    acm_certificate_arn             = local.acm_certificate_arn
    ssl_support_method              = "sni-only"
    minimum_protocol_version        = "TLSv1.2_2021"
  }

  custom_error_response {
    error_caching_min_ttl = "0"
    error_code            = "403"
    response_code         = "200"
    response_page_path    = "/"
  }

  custom_error_response {
    error_caching_min_ttl = "0"
    error_code            = "404"
    response_code         = "200"
    response_page_path    = "/"
  }

  custom_error_response {
    error_caching_min_ttl = "0"
    error_code            = "400"
    response_code         = "200"
    response_page_path    = "/"
  }
  
  depends_on = [
    aws_s3_bucket.default
  ]

  tags = merge(
    local.tags,
    {
      Name   = "${var.s3_bucket_name}" 
      Region = "us-east-1"    
    }
  )
}
