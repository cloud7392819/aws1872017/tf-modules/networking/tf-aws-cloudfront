# ws@2023 acm.tf

resource "aws_acm_certificate" "default" {

  # This allows instances to be created, updated, and deleted. 
  # Instances also support provisioning: https://www.terraform.io/docs/provisioners/index.html
  count = (local.create && var.acm_enabled) ? 1 : 0 
  
  provider          = aws.virginia

  domain_name       = "${var.s3_bucket_name}"
  
  validation_method = "DNS"
  
  lifecycle {
    create_before_destroy = true
  }

  depends_on = [ 
    aws_s3_bucket.default
  ]

  tags = merge(
    local.tags,
    {
      Region  = "us-east-1"      
    }
  )
}


resource "aws_acm_certificate_validation" "default" {

  # This allows instances to be created, updated, and deleted. 
  # Instances also support provisioning: https://www.terraform.io/docs/provisioners/index.html
  count = (local.create && var.acm_enabled) ? 1 : 0   
  
  provider = aws.virginia
  
  certificate_arn         = join("", aws_acm_certificate.default.*.arn)

  validation_record_fqdns = [ 
    join("", aws_route53_record.cert_validation_default.*.fqdn) 
  ]

  depends_on = [
    aws_s3_bucket.default
  ]
}

